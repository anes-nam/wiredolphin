import sys, getopt, os
import textwrap

# structures
network_dict = {
	"0800": "IPv4",
	"0805": "X.25 niveau 3",
	"0806": "ARP",
	"8035": "RARP",
	"8098": "Appletalk",
	"86DD": "IPv6",
}

ip_option_dict = {
	0: "End of Options List",
	1: "No Operation",
	7: "Record Route",
	68: "Time Stamp",
	131: "Loose Routing",
	137: "Strict Routing",
}

tcp_option_dict = {
	"00": "End of Option List",
	"01": "No-Operation",
	"02": "Maximum Segment Size",
	"03": "WSOPT - Window Scale",
	"08": "TSOPT - Time Stamp Option"
}

transport_dict = {
	1: "ICMP",
	2: "IGMP",
	6: "TCP",
	8: "EGP",
	9: "IGP",
	17: "UDP",
	36: "XTP",
	46: "RSVP",
}

hex_letters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']

htmlDic = ['HTTP/1.1', 'HTTP', 'GET', 'HEAD', 'POST', 'PUT', 'DELETE']

def is_hex(x):
 	return x in hex_letters

def split_frames(text):
	# respecting the 11 rules in the instruction, part 1/ Entree
	frames = []
	tmp_frame = ""
	text = text.lower()		# (6)
	lines = text.splitlines()
	expected_offset = "0000"
	erroneous_lines = []
	line_number = 0

	for line in lines:
		line_number += 1
		cursor = 4
		offset = line[:cursor]

		# offset valid?
		if (offset == "0000"):
			if tmp_frame != "":
				frames.append(tmp_frame)				
				tmp_frame = ""
		elif (offset != expected_offset):	# (9)
			erroneous_lines.append((line_number, line))
			continue	# (10)

		# read bytes
		tmp_line = ""
		bytes_read = 0
		while line[cursor+1].isspace():
			cursor += 1
		while (valid_byte(line, cursor)):	# (7) (8)
			bytes_read += 1
			tmp_line = tmp_line + line[cursor+1] + line[cursor+2]
			cursor += 3

		tmp_frame += tmp_line
		expected_offset = str(hex(int(offset, 16)+bytes_read)[2:].zfill(4))
	
	# last frame
	if tmp_frame != "":
		frames.append(tmp_frame)

	return frames, erroneous_lines

def valid_byte(line, cursor):
 	return (len(line) > cursor + 2 and line[cursor].isspace()
		and is_hex(line[cursor+1]) and is_hex(line[cursor+2]))

def read_n_byte(string, n):
	first = string[:2*n]
	second = string[2*n:]
	return first, second

def xtob(hex, padding):
	return bin(int(hex, 16))[2:].zfill(padding)

def dtob(dec, padding):
	return bin(dec)[2:].zfill(padding)

def dtox(dec, padding):
	return hex(dec)[2:].zfill(padding)









