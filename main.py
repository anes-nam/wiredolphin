from utilities import *
import webbrowser

def main(argv):
	input_file = ''
	output_file = ''
	# read input, output filenames from command line arguments
	try:
		opts, _ = getopt.getopt(argv,"hi:o:")
	except Exception:
		print( 'Usage: python main.py -i <input_file> -o <output_file>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('python main.py -i <input_file> -o <output_file>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			input_file = arg
		elif opt in ("-o", "--ofile"):
			output_file = arg
	# print( 'Input file is', input_file)
	if (output_file == ""):
		output_file = "output_" + input_file
	input_path = os.path.join(os.getcwd(), "traces", input_file)
	output_path = os.path.join(os.getcwd(), "resultats", output_file)
	error_message = "Input/output error [verify your input file]"
	try:
		with open(input_path, "r") as f_in:
			with open(output_path, "w") as f_out:
				frames, erroneous_lines = split_frames(f_in.read())
				if erroneous_lines:
					print("Erroneous lines:")
					for (nb, line) in erroneous_lines:
						print("Line " + str(nb) + ": " + line)
					print("Frames with erroneous lines are ignored.")
				for frame in frames:
					f_out.write("\n==== Frame No("+str(frames.index(frame)+1)+")==========\nAnalyze frame\n")
					analyze(frame, f_out)
				print( "Output file is", output_file)
				# will open result file in the default .txt app
				# webbrowser.open('resultats/'+output_file)
	except IOError:
		print(error_message)

def analyze(frame, f_out):
	try:
		# couche liaison
		header_eth = frame[0:28]
		res_type = ethernet(header_eth, f_out)
		if res_type != '0800':
			msg = 'Unsupported protocol '+ network_dict[res_type]
			f_out.write("\n\n" + msg)
			raise ValueError(msg)
		# couche reseau
		paquet = frame[28:]
		segment, protocol = ip(paquet, f_out)
		# couche transport
		if protocol != 6 :
			msg = 'Unsupported protocol ' + transport_dict[protocol]
			f_out.write("\n\n" + msg)
			raise ValueError(msg)
		hypertext = TCP(segment, f_out)
		# couche application
		http(hypertext, f_out)
	except (ValueError, KeyError) as e:
		print("Malformed frame. Exception: " + str(e))

def ethernet(header_eth, f_out):
	f_out.write("\nEthernet")
	# MAC Destination
	dst_mac = header_eth[0:12]
	f_out.write("\n\tDestination: " + ':'.join(textwrap.wrap(dst_mac, 2)))
	# MAC Source
	src_mac = header_eth[12:24]
	f_out.write("\n\tSource: " + ':'.join(textwrap.wrap(src_mac, 2)))
	# Type
	res_type = header_eth[24:28]
	f_out.write("\n\tType: " + network_dict[res_type] + " (0x" + res_type + ")")
	# return couche reseau type
	return res_type

def ip(paquet, f_out):
	f_out.write("\n\nInternet Protocol")
	# IP version
	ip_version = paquet[0:1]
	f_out.write("\n\t" + str(xtob(ip_version, 4)) + " .... = Version: " + str(int(ip_version, 16)))
	# Header Length
	header_length = paquet[1:2]
	f_out.write("\n\t.... " + str(xtob(header_length, 4)) + " = Header Length: "
		+ str(int(header_length, 16) * 4) + " bytes (" + str(int(header_length, 16)) + ")")
	# TOS - Is it worth it to code this properly, knowing that it is never used?
	# tos = paquet[2:4]
	f_out.write("\n\tDifferentiated Services Field:")
	f_out.write("\n\t\t0000 00.. = Differentiated Services Codepoint: Default (0)")
	f_out.write("\n\t\t.... ..00 = Explicit Congestion Notification: Not ECN-Capable Transport (0)")
	# Total Length
	total_length = paquet[4:8]
	f_out.write("\n\tTotal Length: 0x" + total_length + " (" + str(int(total_length, 16)) + ")")
	# Identification
	identifier = paquet[8:12]
	f_out.write("\n\tIdentification: 0x" + identifier + " (" + str(int(identifier, 16)) + ")")
	f_out.write("\n\tFlags: ")
	# Fragmentation
	tmp = paquet[12:16]
	tmp = xtob(tmp, 16)
	r = tmp[0]
	df = tmp[1]
	mf = tmp[2]
	fo = tmp[3:]
	f_out.write("\n\t\t" + r + "... .... .... .... = Reserved bit: Not set")
	f_out.write("\n\t\t." + df + ".. .... .... .... = Don't fragment: " + ("Not set" if df == "0" else "Set"))
	f_out.write("\n\t\t.." + mf + ". .... .... .... = More fragments: " + ("Not set" if mf == "0" else "Set"))
	f_out.write("\n\t\t..." + fo[0:1] + " " + fo[1:5] + " " + fo[5:9] + " " + fo[9:13] + " = Fragment offset: " + str(int(fo, 2)))
	# Time to Live
	ttl = paquet[16:18]
	f_out.write("\n\tTime to Live: " + str(int(ttl, 16)))
	# Protocol
	protocol = paquet[18:20]
	protocol = int(protocol, 16)
	f_out.write("\n\tProtocol: " + transport_dict[protocol] + " (" + str(protocol) + ")")
	# Header checksum
	header_checksum = paquet[20:24]
	f_out.write("\n\tHeader checksum: 0x" + header_checksum)
	# Source (IP address)
	source = paquet[24:32]
	f_out.write("\n\tSource: "
		+ str(int(source[0:2], 16)) + "."
		+ str(int(source[2:4], 16)) + "."
		+ str(int(source[4:6], 16)) + "."
		+ str(int(source[6:8], 16)))
	# Destination (IP address)
	destination = paquet[32:40]
	f_out.write("\n\tDestination: "
		+ str(int(destination[0:2], 16)) + "."
		+ str(int(destination[2:4], 16)) + "."
		+ str(int(destination[4:6], 16)) + "."
		+ str(int(destination[6:8], 16)))
	# Options
	option_end = int(header_length, 16) * 4
	cursor_end = option_end * 2
	if (option_end > 20):
		f_out.write("\n\tOptions: (" + str(option_end - 20) + ")")
	cursor = 40
	while (cursor < cursor_end):
		option_type = int(paquet[cursor:cursor+2], 16)
		# "End of Options List"
		if (option_type == 0):
			f_out.write("\n\t\tIP Option - " + ip_option_dict[option_type] + " (EOL)")
			cursor += 2
			break
		# "No Operation"
		if (option_type == 1):
			f_out.write("\n\t\tIP Option - " + ip_option_dict[option_type])
			cursor += 2
			continue
		option_length = int(paquet[cursor+2:cursor+4], 16) * 2
		if option_type in ip_option_dict:
			f_out.write("\n\t\tIP Option - " + ip_option_dict[option_type] + " (" + str(int(option_length/2)) + " bytes)")
		option_content = paquet[cursor:cursor + option_length]
		# Record Route
		if (option_type == 7):
			option_pointer = int(option_content[4:6], 16)
			f_out.write("\n\t\t\tType: 7")
			f_out.write("\n\t\t\tLength: " + str(int(option_length/2)))
			f_out.write("\n\t\t\tPointer: " + str(option_pointer))
			option_cursor = 6
			while option_cursor < option_length:
				ip = option_content[option_cursor:option_cursor+8]
				if (ip == "00000000"):
					f_out.write("\n\t\t\tEmpty Route: 0.0.0.0")
				else:
					f_out.write("\n\t\t\tRecorded Route: "
						+ str(int(ip[0:2], 16)) + "."
						+ str(int(ip[2:4], 16)) + "."
						+ str(int(ip[4:6], 16)) + "."
						+ str(int(ip[6:8], 16)))
				if option_cursor == option_pointer * 2:
					f_out.write(" <- (next)")
				option_cursor += 8
		# Timestamp
		elif (option_type == 68):
			option_pointer = int(option_content[4:6], 16)
			f_out.write("\n\t\t\tType: 68")
			f_out.write("\n\t\t\tLength: " + str(int(option_length/2)))
			f_out.write("\n\t\t\tPointer: " + str(option_pointer))
			option_cursor = 8
			while option_cursor < option_length:
				ts = option_content[option_cursor:option_cursor+8]
				f_out.write("\n\t\t\tTime stamp: " + str(int(ts, 16)))
				option_cursor += 8
		# Loose Routing / Strict Routing
		elif (option_type in [131, 137]):
			option_pointer = int(option_content[4:6], 16)
			f_out.write("\n\t\t\tType: " + str(option_type))
			f_out.write("\n\t\t\tLength: " + str(int(option_length/2)))
			f_out.write("\n\t\t\tPointer: " + str(option_pointer))
			option_cursor = 6
			while option_cursor < option_pointer * 2:
				ip = option_content[option_cursor:option_cursor+8]
				f_out.write("\n\t\t\tRecorded Route: "
					+ str(int(ip[0:2], 16)) + "."
					+ str(int(ip[2:4], 16)) + "."
					+ str(int(ip[4:6], 16)) + "."
					+ str(int(ip[6:8], 16)))
				if option_cursor == option_pointer * 2:
					f_out.write(" <- (next)")
				option_cursor += 8
			while option_cursor < option_length - 8:
				ip = option_content[option_cursor:option_cursor+8]
				f_out.write("\n\t\t\tSource Route: "
					+ str(int(ip[0:2], 16)) + "."
					+ str(int(ip[2:4], 16)) + "."
					+ str(int(ip[4:6], 16)) + "."
					+ str(int(ip[6:8], 16)))
				option_cursor += 8
			ip = option_content[option_cursor:option_cursor+8]
			f_out.write("\n\t\t\tDestination: "
				+ str(int(ip[0:2], 16)) + "."
				+ str(int(ip[2:4], 16)) + "."
				+ str(int(ip[4:6], 16)) + "."
				+ str(int(ip[6:8], 16)))
		else:
			f_out.write("\n\tUnrecognized IP option")
		cursor += option_length	
	return paquet[cursor:], protocol

def TCP(segment, f_out):
	f_out.write("\n\nTransmission Control Protocol")
	# Ports
	f_out.write("\n\tSource Port: " + str(int(segment[0:4], 16)))
	f_out.write("\n\tDestination Port: " + str(int(segment[4:8], 16)))
	 
	# Sequence number
	sn = int(segment[8:16], 16)
	f_out.write("\n\tSequence number: " + str(sn))

	# Acknowledgment
	an = int(segment[16:24], 16)
	f_out.write("\n\tAcknowledgment number: " + str(an))

	# THL
	header_length = segment[24:25]
	f_out.write("\n\t" + str(xtob(header_length, 4))
		+ " .... = Header Length: " + str(4 * int(header_length, 16))
		+ " bytes ("+ str(header_length) +")")

	# Flags
	dec_flags = int(segment[25:28], 16)
	f_out.write("\n\tFlags: 0x" + dtox(dec_flags, 3))
	flags = [int(d) for d in dtob(dec_flags, 12)]
	state = ["Not set", "Set"]
	f_out.write("\n\t\t000. .... .... = Reserved: Not set")
	f_out.write("\n\t\t..." + str(flags[3]) + " .... .... = Nonce: " + state[flags[3]])
	f_out.write("\n\t\t.... " + str(flags[4]) + "... .... = Congestion Window Reduced (CWR): " + state[flags[4]])
	f_out.write("\n\t\t.... ." + str(flags[5]) + ".. .... = ECN-Echo: " + state[flags[5]])
	f_out.write("\n\t\t.... .." + str(flags[6]) + ". .... = Urgent: " + state[flags[6]])
	f_out.write("\n\t\t.... ..." + str(flags[7]) + " .... = Acknowledgment:" + state[flags[7]])
	f_out.write("\n\t\t.... .... " + str(flags[8]) + "... = Push:" + state[flags[8]])
	f_out.write("\n\t\t.... .... ." + str(flags[9]) + ".. = Reset: " + state[flags[9]])
	f_out.write("\n\t\t.... .... .." + str(flags[10]) + ". = Syn: " + state[flags[10]])
	f_out.write("\n\t\t.... .... ..." + str(flags[11]) + " = Fin: " + state[flags[11]])
	
	# Window
	window = int(segment[28:32], 16)
	f_out.write("\n\tWindow size value: "+ str(window))

	# Checksum
	checksum = segment[32:36]
	f_out.write("\n\tChecksum: 0x" + str(checksum))

	# Urgent pointer
	urp = int(segment[36:40], 16)
	f_out.write("\n\tUrgent pointer: " + str(urp))
	
	# Options
	option_end = int(header_length, 16) * 4
	cursor_end = option_end * 2 # 1 bytes = 2 lettres hex
	if (option_end > 20):
		f_out.write("\n\tOptions: (" + str(option_end - 20) + " bytes)")
	cursor = 40	# start reading options here\
	while (cursor < cursor_end):
		option_type = segment[cursor:cursor+2]
		if (option_type == "00"):
			f_out.write("\n\t\tTCP Option - " + tcp_option_dict[option_type] + " (EOL)")
			cursor += 2
			break
		if (option_type == "01"):
			f_out.write("\n\t\tTCP Option - " + tcp_option_dict[option_type] + " (NOP)")
			cursor += 2
			continue
		option_length = segment[cursor+2:cursor+4]
		option_length = int(option_length, 16) * 2
		if option_type in tcp_option_dict:
			f_out.write("\n\t\tTCP Option - " + tcp_option_dict[option_type] + " (" + str(int(option_length/2)) + " bytes)")
		option_content = segment[cursor:cursor + option_length]
		# Maximum segment size
		if option_type == "02":
			f_out.write("\n\t\t\tKind: Maximum Segment Size (2)")
			f_out.write("\n\t\t\tLength: 4")
			f_out.write("\n\t\t\tMSS: " + str(int(option_content[4:8], 16)))
		# WSOPT - Window Scale
		elif option_type == "03":
			f_out.write("\n\t\t\tKind: Window Sclae (3)")
			f_out.write("\n\t\t\tLength: 3")
			f_out.write("\n\t\t\tShift count: " + str(int(option_content[4:6], 16)))
		# Sack Permitted
		elif option_type == "04":
			f_out.write("\n\t\t\tKind: SACK Permitted (4)")
			f_out.write("\n\t\t\tLength: 2")
		# TSOPT - Time Stamp Option
		elif option_type == "08":
			f_out.write("\n\t\t\tKind: Time Stamp Option (8)")
			f_out.write("\n\t\t\tLength: 10")
			f_out.write("\n\t\t\tTimestamp value: " + str(int(option_content[4:12], 16)))
			f_out.write("\n\t\t\tTimestamp echo reply: " + str(int(option_content[12:20], 16)))
		else:
			f_out.write("\n\t\tUnrecognized option")
			f_out.write("\n\t\t\tRaw hex: 0x" + option_content)
		cursor += option_length
	hyper = segment[cursor_end:]
	return hyper

def http(hypertext, f_out):
	if len(hypertext) == 0 or hypertext.isspace():
		return
	try:
		lines = [] 
		for l in hypertext.split('0d0a'):
			try:
				lines.append(bytearray.fromhex(l).decode())
			except Exception as e:
				continue
		first_line = lines[0]
		type_requet = first_line.split(' ')[0]
		f_out.write("\n\nHypertext Transfer Protocol")
		if type_requet not in htmlDic :
			print("HTTP : type non reconnu")
			return 
		elif type_requet == 'HTML' : 
			f_out.write("\n\t" + first_line)
			f_out.write("\n\t\t[Expert Info(Chat/Sequence):"+ first_line +"]")
			f_out.write("\n\t\t\t["+first_line+"]")
			f_out.write("\n\t\t\t[Severity level: Chat]")
			f_out.write("\n\t\t\t[Group: Sequence]")
			f_out.write("\n\t\tResponse Version: "+first_line.split()[0])
			f_out.write("\n\t\tStatus Code: "+first_line.split()[1])
			f_out.write("\n\t\t[Status Code Description: "+first_line.split()[-1])
			f_out.write("\n\t\tResponse Phrase: "+first_line.split()[-1])
		else : 
			f_out.write("\n\t"+ first_line)
			f_out.write("\n\t\t[Expert Info (Chat/Sequence): " + first_line + "]")
			f_out.write("\n\t\t\t[" + first_line + "]")
			f_out.write("\n\t\t\t[Severity level: Chat]")
			f_out.write("\n\t\t\t[Group: Sequence]")
			f_out.write("\n\t\tRequest Method: "+type_requet)
			f_out.write("\n\t\tRequest URI: "+first_line.split()[1])
			f_out.write("\n\t\tRequest Version: "+first_line.split()[-1])
		for line in lines[1:] :
			f_out.write("\n\t" + line)
			if 'Referer: ' in line:
				f_out.write("\n\t[Full request URI: "
				+ line.replace('Referer: ', '')
				+ first_line.split()[1][1:]+"]")
	except Exception as e:
	 	print("HTTP probleme : "+str(e))

if __name__ == "__main__":
   main(sys.argv[1:])


